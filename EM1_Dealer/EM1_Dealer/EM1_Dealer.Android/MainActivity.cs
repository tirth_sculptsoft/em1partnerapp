﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using ImageCircle.Forms.Plugin.Abstractions;
using ImageCircle.Forms.Plugin.Droid;
using Plugin.FirebasePushNotification;
using Plugin.FirebasePushNotification.Abstractions;
using Android.Content;

namespace EM1_Dealer.Droid
{
    [Activity(Label = "EM1_Dealer", Icon = "@drawable/icon",
        Theme = "@style/MainTheme", LaunchMode = LaunchMode.SingleTop,
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
        ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);
            global::Xamarin.Forms.Forms.Init(this, bundle);

            LoadApplication(new App());

            ImageCircleRenderer.Init();
            FirebasePushNotificationManager.ProcessIntent(Intent);
        }
        protected override void OnNewIntent(Intent intent)
        {
            base.OnNewIntent(intent);
            FirebasePushNotificationManager.ProcessIntent(intent);
        }
    }
}

