﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading;
using Plugin.FirebasePushNotification;
using Android.Content.PM;
using Android.Support.V7.App;

namespace EM1_Dealer.Droid
{
    [Activity(Label = "EM1 Partner",Theme = "@style/MainTheme.Splash",
        MainLauncher = true,NoHistory = true,
        ScreenOrientation = ScreenOrientation.Portrait)]
    public class SplashActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            var mainIntent = new Intent(Application.Context, typeof(MainActivity));

            if(Intent.Extras !=null)
            {
                mainIntent.PutExtras(Intent.Extras);
            }

            mainIntent.SetFlags(ActivityFlags.SingleTop);

            StartActivity(mainIntent);
        }
        protected override void OnResume()
        {
            base.OnResume();
        }
    }
}