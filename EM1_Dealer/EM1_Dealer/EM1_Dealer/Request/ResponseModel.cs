﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Request
{
    public class ResponseModel
    {
        public int StatusCode { get; set; }
        public string ResponseJsonString { get; set; }
    }
}
