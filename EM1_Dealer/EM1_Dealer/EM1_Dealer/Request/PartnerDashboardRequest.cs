﻿using EM1_Dealer.Models.Response;
using EM1_Dealer.Resources;
using EM1_Dealer.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Request
{
    public class PartnerDashboardRequest : BaseRequest
    {
        public async Task<Response_PartnerDashboard> Execute()
        {
            Response_PartnerDashboard response = new Response_PartnerDashboard();

            try
            {
                var res = await Get(UrlGenerator.PartnerDashboardURL());
                response = JsonConvert.DeserializeObject<Response_PartnerDashboard>(res.ResponseJsonString);
            }

            catch (Exception ex)
            {
                Debug.WriteLine(AppResources.ExceptionHeaderMessage + ex.Message);
            }

            return response;
        }
    }
}
