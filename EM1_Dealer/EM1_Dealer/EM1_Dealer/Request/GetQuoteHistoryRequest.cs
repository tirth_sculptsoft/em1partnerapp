﻿using EM1_Dealer.Models.Response;
using EM1_Dealer.Resources;
using EM1_Dealer.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Request
{
    public class GetQuoteHistoryRequest : BaseRequest
    {
        public async Task<Response_GetClosedQuote> Execute()
        {
            Response_GetClosedQuote response = new Response_GetClosedQuote();

            try
            {
                var res = await Get(UrlGenerator.GetQuoteHistoryURL());
                response = JsonConvert.DeserializeObject<Response_GetClosedQuote>(res.ResponseJsonString);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(AppResources.ExceptionHeaderMessage + ex.Message);
            }

            return response;
        }
    }
}
