﻿using EM1_Dealer.Enums;
using EM1_Dealer.Models.Request;
using EM1_Dealer.Models.Response;
using Newtonsoft.Json;
using EM1_Dealer.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EM1_Dealer.Resources;

namespace EM1_Dealer.Request
{
    public class UpdateQuoteRequest : BaseRequest
    {
        public async Task<Response_UpdateQuote> Execute(Request_UpdateQuote request)
        {
            Response_UpdateQuote response = new Response_UpdateQuote();

            try
            {
                string jsonstring = JsonConvert.SerializeObject(request);
                var res = await Put(UrlGenerator.UpdateQuoteURL(), jsonstring);

                response = JsonConvert.DeserializeObject<Response_UpdateQuote>(res.ResponseJsonString);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(AppResources.ExceptionHeaderMessage + ex.Message);
            }

            return response;
        }
    }
}
