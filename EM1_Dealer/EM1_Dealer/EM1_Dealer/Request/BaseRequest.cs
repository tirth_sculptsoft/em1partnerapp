﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Request
{
    public class BaseRequest
    {
        HttpClient Client;

        public BaseRequest()
        {
            Client = new HttpClient();
            Client.MaxResponseContentBufferSize = 256000;
        }

        protected async Task<ResponseModel> Get(string Url)
        {
            var u = string.Concat(Url);
            ResponseModel resModel = new ResponseModel();
            try
            {
                var res = await Client.GetAsync(string.Concat(Url));
                if (res.IsSuccessStatusCode)
                {
                    resModel.StatusCode = 1;
                    var content = await res.Content.ReadAsStringAsync();
                    resModel.ResponseJsonString = content;
                }
                else
                {
                    resModel.StatusCode = (int)res.StatusCode;
                }
            }
            catch (Exception e)
            {
                resModel.StatusCode = -1;
                resModel.ResponseJsonString = e.Message;
            }
            return resModel;
        }

        protected async Task<ResponseModel> Post(string Url, string Payload)
        {
            ResponseModel resModel = new ResponseModel();
            try
            {
                var content = new StringContent(Payload, Encoding.UTF8, "application/json");

                var res = await Client.PostAsync(Url, content);
                if (res.IsSuccessStatusCode)
                {
                    resModel.StatusCode = 1;
                    var rescontent = await res.Content.ReadAsStringAsync();
                    resModel.ResponseJsonString = rescontent;
                }
                else
                {
                    resModel.StatusCode = (int)res.StatusCode;

                }
            }
            catch (Exception e)
            {
                resModel.StatusCode = -1;
                resModel.ResponseJsonString = e.Message;
            }
            return resModel;
        }

        protected async Task<ResponseModel> Put(string Url, string Payload)
        {
            ResponseModel resModel = new ResponseModel();
            try
            {
                var content = new StringContent(Payload, Encoding.UTF8, "application/json");

                var res = await Client.PutAsync(Url, content);
                if(res.IsSuccessStatusCode)
                {
                    resModel.StatusCode = 1;
                    var rescontent = await res.Content.ReadAsStringAsync();
                    resModel.ResponseJsonString = rescontent;
                }
            }
            catch (Exception e)
            {
                resModel.StatusCode = -1;
                resModel.ResponseJsonString = e.Message;
            }

            return resModel;
        }
    }
}
