﻿using EM1_Dealer.Enums;
using EM1_Dealer.Models.Response;
using EM1_Dealer.Resources;
using EM1_Dealer.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Request
{
    public class GetPendingQuoteRequest : BaseRequest
    {
        public async Task<Response_GetPendingQuote> Execute()
        {
            Response_GetPendingQuote response = new Response_GetPendingQuote();

            try
            {
                var res = await Get(UrlGenerator.GetReqQuoteURL());
                response = JsonConvert.DeserializeObject<Response_GetPendingQuote>(res.ResponseJsonString);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(AppResources.ExceptionHeaderMessage + ex.Message);
            }

            return response;
        }
    }
}
