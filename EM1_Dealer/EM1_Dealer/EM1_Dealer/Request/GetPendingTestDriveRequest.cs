﻿using EM1_Dealer.Models.Response;
using EM1_Dealer.Resources;
using EM1_Dealer.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Request
{
    public class GetPendingTestDriveRequest : BaseRequest
    {
        public async Task<Response_GetPendingTestDrive> Excecute()
        {
            Response_GetPendingTestDrive response = new Response_GetPendingTestDrive();

            try
            {
                var res = await Get(UrlGenerator.GetReqTestDriveURL());
                response = JsonConvert.DeserializeObject<Response_GetPendingTestDrive>(res.ResponseJsonString);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(AppResources.ExceptionHeaderMessage + ex.Message);
            }

            return response;
        }
    }
}
