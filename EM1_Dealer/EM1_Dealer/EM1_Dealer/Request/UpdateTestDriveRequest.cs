﻿using EM1_Dealer.Models.Request;
using EM1_Dealer.Models.Response;
using EM1_Dealer.Resources;
using EM1_Dealer.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Request
{
    public class UpdateTestDriveRequest : BaseRequest
    {
        public async Task<Response_UpdateTestDrive> Execute(Request_UpdateTestDrive request)
        {
            Response_UpdateTestDrive response = new Response_UpdateTestDrive();

            try
            {
                string jsonstring = JsonConvert.SerializeObject(request);
                var res = await Put(UrlGenerator.UpdateTestDriveURL(), jsonstring);

                response = JsonConvert.DeserializeObject<Response_UpdateTestDrive>(res.ResponseJsonString);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(AppResources.ExceptionHeaderMessage + ex.Message);
            }

            return response;
        }
    }
}
