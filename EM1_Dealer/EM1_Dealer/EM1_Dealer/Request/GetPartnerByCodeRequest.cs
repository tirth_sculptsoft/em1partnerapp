﻿using EM1_Dealer.Models.Response;
using EM1_Dealer.Resources;
using EM1_Dealer.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Request
{
    public class GetPartnerByCodeRequest : BaseRequest
    {
        public async Task<Response_GetPartnerByCode> Execute(string partnercode)
        {
            Response_GetPartnerByCode response = new Response_GetPartnerByCode();

            try
            {
                var res = await Get(UrlGenerator.GetPartnerByCodeURL(partnercode));
                response = JsonConvert.DeserializeObject<Response_GetPartnerByCode>(res.ResponseJsonString);
            }

            catch (Exception ex)
            {
                Debug.WriteLine(AppResources.ExceptionHeaderMessage + ex.Message);
            }

            return response;
        }
    }
}
