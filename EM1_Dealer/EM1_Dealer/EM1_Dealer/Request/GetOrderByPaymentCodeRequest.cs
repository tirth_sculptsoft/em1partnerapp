﻿using EM1_Dealer.Models.Response;
using EM1_Dealer.Resources;
using EM1_Dealer.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Request
{
    public class GetOrderByPaymentCodeRequest : BaseRequest
    {
        public async Task<Response_GetOrderByPaymentCode> Execute(string paymentcode)
        {
            Response_GetOrderByPaymentCode response = new Response_GetOrderByPaymentCode();

            try
            {
                var res = await Get(UrlGenerator.GetOrderByPaymentCodeURL(paymentcode));
                response = JsonConvert.DeserializeObject<Response_GetOrderByPaymentCode>(res.ResponseJsonString);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(AppResources.ExceptionHeaderMessage + ex.Message);
            }

            return response;
        }
    }
}
