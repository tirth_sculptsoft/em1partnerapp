﻿using EM1_Dealer.Models.Request;
using EM1_Dealer.Models.Response;
using EM1_Dealer.Resources;
using EM1_Dealer.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Request
{
    public class UpdateOrderStatusRequest : BaseRequest
    {
        public async Task<Response_UpdateOrderStatus> Execute(Request_UpdateOrderStatus request)
        {
            Response_UpdateOrderStatus response = new Response_UpdateOrderStatus();

            try
            {
                string jsonstring = JsonConvert.SerializeObject(request);
                var res = await Put(UrlGenerator.UpdateOrderStatusURL(), jsonstring);

                response = JsonConvert.DeserializeObject<Response_UpdateOrderStatus>(res.ResponseJsonString);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(AppResources.ExceptionHeaderMessage + ex.Message);
            }

            return response;
        }
    }
}
