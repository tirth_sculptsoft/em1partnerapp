﻿using EM1_Dealer.Models.Request;
using EM1_Dealer.Models.Response;
using EM1_Dealer.Resources;
using EM1_Dealer.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Request
{
    public class PartnerUserLoginRequest : BaseRequest
    {
        public async Task<Response_PartnerUserLogin> Execute(Request_PartnerUserLogin request)
        {
            Response_PartnerUserLogin response = new Response_PartnerUserLogin();

            try
            {
                string jsonstring = JsonConvert.SerializeObject(request);
                var res = await Post(UrlGenerator.PartnerUserLoginURL(), jsonstring);

                response = JsonConvert.DeserializeObject<Response_PartnerUserLogin>(res.ResponseJsonString);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(AppResources.ExceptionHeaderMessage + ex.Message);
            }

            return response;
        }
    }
}
