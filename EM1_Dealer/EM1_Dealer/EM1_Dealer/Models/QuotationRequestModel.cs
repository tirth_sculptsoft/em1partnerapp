﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Models
{
    public class QuotationRequestModel
    {
        public string QuoteID { get; set; }
        public string CustomerName { get; set; }
        public string QuoteReqDateTime { get; set; }
        public string PreferTimeSlot { get; set; }
        public string QuoteClosedStatus { get; set; }
        public string QuoteReqImage { get; set; }
        public string VehicleModelName { get; set; }
        public string VehicleVersionName { get; set; }
        public string ModelColor { get; set; }
        public string QuoteStatus { get; set; }
        public string PartnerRemarks { get; set; }
        public string StatusColor { get; set; }
    }
}
