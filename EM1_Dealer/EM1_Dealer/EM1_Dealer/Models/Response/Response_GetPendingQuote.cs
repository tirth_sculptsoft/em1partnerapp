﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Models.Response
{
    public class Response_GetPendingQuote : BaseResponseModel
    {
        public PendingQuoteList data { get; set; }
        public string requestData { get; set; }
    }
    public class PendingQuoteList
    {
        public List<PendingQuote> list { get; set; }
        public string result { get; set; }
    }

    public class PendingQuote
    {
        public int quoteId { get; set; }
        public string activityCode { get; set; }
        public string fullname { get; set; }
        public DateTime createdDate { get; set; }
        public string productName { get; set; }
        public string categoryCode { get; set; }
        public string brandCode { get; set; }
        public string modelCode { get; set; }
        public string varientCode { get; set; }
        public string varientColor { get; set; }
        public string status { get; set; }
        public bool isGiven { get; set; }
        public int result { get; set; }
        public PerferTimeSlot timeSlot { get; set; }
    }

    public class PerferTimeSlot
    {
        public string from { get; set; }
        public string to { get; set; }
    }
}
