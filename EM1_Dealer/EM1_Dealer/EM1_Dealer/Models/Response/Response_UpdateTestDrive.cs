﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Models.Response
{
    public class Response_UpdateTestDrive : BaseResponseModel
    {
        public TDData data { get; set; }
    }
    public class TDData
    {
        public int result { get; set; }
    }
}
