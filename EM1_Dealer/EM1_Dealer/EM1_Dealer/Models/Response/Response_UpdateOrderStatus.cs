﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Models.Response
{
    public class Response_UpdateOrderStatus : BaseResponseModel
    {
        public PaymentOrderData data { get; set; }
    }
    public class PaymentOrderData
    {
        public int result { get; set; }
    }
}
