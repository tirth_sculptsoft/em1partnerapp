﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Models.Response
{
    public class Response_UpdateQuote : BaseResponseModel
    {
        public Data data { get; set; }
    }
    public class Data
    {
        public int result { get; set; }
    }
}
