﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Models.Response
{
    public class Response_GetClosedQuote : BaseResponseModel
    {
        public QuoteHistoryData data { get; set; }
    }
    public class PreferHistoryTimeSlot
    {
        public string from { get; set; }
        public string to { get; set; }
    }

    public class QuoteHistoryList
    {
        public int quoteId { get; set; }
        public string activityCode { get; set; }
        public string fullname { get; set; }
        public DateTime createdDate { get; set; }
        public string productName { get; set; }
        public string categoryCode { get; set; }
        public string brandCode { get; set; }
        public string modelCode { get; set; }
        public string varientCode { get; set; }
        public string varientColor { get; set; }
        public string status { get; set; }
        public DateTime updatedDate { get; set; }
        public string partnerRemarks { get; set; }
        public bool isGiven { get; set; }
        public PreferHistoryTimeSlot timeSlot { get; set; }
        public int activityId { get; set; }
        public int partnerId { get; set; }
        public int result { get; set; }
    }

    public class QuoteHistoryData
    {
        public List<QuoteHistoryList> list { get; set; }
        public int result { get; set; }
    }

}
