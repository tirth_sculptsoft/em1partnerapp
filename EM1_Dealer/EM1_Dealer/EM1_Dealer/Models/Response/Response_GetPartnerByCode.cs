﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Models.Response
{
    public class Response_GetPartnerByCode : BaseResponseModel
    {
        public PartnerData data { get; set; }
    }
    public class PartnerData
    {
        public List<PartnerList> list { get; set; }
        public int result { get; set; }
    }
    public class PartnerList
    {
        public int pid { get; set; }
        public string name { get; set; }
        public string partnerCode { get; set; }
    }
}
