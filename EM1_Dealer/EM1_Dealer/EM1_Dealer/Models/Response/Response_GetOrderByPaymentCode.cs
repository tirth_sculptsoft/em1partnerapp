﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Models.Response
{
    public class Response_GetOrderByPaymentCode : BaseResponseModel
    {
        public OrderData data { get; set; }
    }
    public class OrderList
    {
        public long paymentCodeId { get; set; }
        public double paymentamout { get; set; }
        //public double amount { get; set; }
        public string prodCat { get; set; }
        public string prodBrand { get; set; }
        public string prodModel { get; set; }
        public string prodColor { get; set; }
        public string prodName { get; set; }
        public int uid { get; set; }
        public string fullname { get; set; }
        public long orderId { get; set; }
        public long paymentId { get; set; }
        public int result { get; set; }
        public bool isamontneeded { get; set; }
    }
    public class OrderData
    {
        public List<OrderList> list { get; set; }
        public int result { get; set; }
    }
}
