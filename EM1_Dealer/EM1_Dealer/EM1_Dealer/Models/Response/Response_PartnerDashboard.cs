﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Models.Response
{
    public class Response_PartnerDashboard : BaseResponseModel
    {
        public DashboardData data { get; set; }
    }

    public class DashboardData
    {
        public int quoteCount { get; set; }
        public int testDriveCount { get; set; }
        public int paymentCount { get; set; }
        public int result { get; set; }
    }
}
