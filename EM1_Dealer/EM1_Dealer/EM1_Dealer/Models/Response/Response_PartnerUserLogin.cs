﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Models.Response
{
    public class Response_PartnerUserLogin : BaseResponseModel
    {
        public PartnerUserData data { get; set; }
    }

    public class PartnerUserData
    {
        public int puid { get; set; }
        public string fname { get; set; }
        public string lname { get; set; }
        public int type { get; set; }
        public int result { get; set; }
    }
}
