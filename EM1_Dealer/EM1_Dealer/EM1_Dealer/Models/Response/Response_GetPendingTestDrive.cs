﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Models.Response
{
    public class Response_GetPendingTestDrive : BaseResponseModel
    {
        public TDdata data { get; set; }
    }
    public class TDdata
    {
        public List<TDList> list { get; set; }
        public int result { get; set; }
    }
    public class TDList
    {
        public int testDriveId { get; set; }
        public string activityCode { get; set; }
        public DateTime createdDate { get; set; }
        public string customerName { get; set; }
        public int activityId { get; set; }
        public int uid { get; set; }
        public int pcid { get; set; }
        public string address { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public string productName { get; set; }
        public string categoryCode { get; set; }
        public string brandCode { get; set; }
        public string modelCode { get; set; }
        public string status { get; set; }
        public DateTime testDriveDate { get; set; }
        public PreferTDTimeSlot testDriveTimeSlot { get; set; }
        public DateTime statusChangedDatetime { get; set; }
        public PreferTDCallTimeSlot timePreferences { get; set; }
        public int result { get; set; }
    }
    public class PreferTDTimeSlot
    {
        public string from { get; set; }
        public string to { get; set; }
    }

    public class PreferTDCallTimeSlot
    {
        public string from { get; set; }
        public string to { get; set; }
    }
}
