﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Models
{
    public class DashboardPendingItem
    {
        public int Id { get; set; }
        public string listbgcolor { get; set; }
        public int pendingcount { get; set; }
        public string pendingtext { get; set; }
        public string pendingtextdesc { get; set; }
    }
}
