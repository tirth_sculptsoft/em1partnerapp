﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Models
{
    public class TestDrive_Status
    {
        public int TDStatusID { get; set; }
        public string TDStatus { get; set; }
    }
}
