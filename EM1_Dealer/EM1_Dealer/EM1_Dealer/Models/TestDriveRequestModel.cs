﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Models
{
    public class TestDriveRequestModel
    {
        public int TDReqID { get; set; }
        public string CustomerName { get; set; }
        public string TDScheduleDate { get; set; }
        public string PreferCallTimeSlot { get; set; }
        public string PreferTestDriveTimeSlot { get; set; }
        public string TDAddress { get; set; }
        public string TestReqDateTime { get; set; }
        public string TestDriveReqImage { get; set; }
        public string VehicleModelName { get; set; }
        public string TDStatus { get; set; }
        public string StatusColor { get; set; }
    }
}
