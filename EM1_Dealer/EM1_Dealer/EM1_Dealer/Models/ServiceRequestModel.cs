﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Models
{
    public class ServiceRequestModel
    {
        public int ServiceID { get; set; }
        public string CustomerName { get; set; }
        public string ServiceReqCreatedDate { get; set; }
        public string ServiceScheduleDate { get; set; }
        public string PreferCallTimeSlot { get; set; }
        public string PreferServiceTimeSlot { get; set; }
        public string ServiceReqImage { get; set; }
        public string VehicleModelName { get; set; }
        public string VehicleModelDetail { get; set; }
        public string LastServiceDate { get; set; }
        public string VehicleDistanceRun { get; set; }
        public string VehicleServiceStatus { get; set; }
        public string StatusColor { get; set; }
    }
}
