﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Models
{
    public class PaymentRequestModel
    {
        public int OrderID { get; set; }
        public int PaymentID { get; set; }
        public string CustomerName { get; set; }
        public string PaymentReqDateTime { get; set; }
        public string PaymentReqImage { get; set; }
        public string VehicleModelName { get; set; }
        public string VehicleVersionName { get; set; }
        public string ModelColor { get; set; }
        public string PaymentTypeText { get; set; }
        public string PaymentType { get; set; }
        public string PaymentAmount { get; set; }
        public string PaymentCode { get; set; }
        public string PaymentStatus { get; set; }
        public string StatusColor { get; set; }
    }
}
