﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Models.Request
{
    public class Request_PartnerUserLogin : BaseRequestModel
    {
        public int partnerId { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string deviceId { get; set; }
        public string notificationtoken { get; set; }
        public string platform { get; set; }
    }
}
