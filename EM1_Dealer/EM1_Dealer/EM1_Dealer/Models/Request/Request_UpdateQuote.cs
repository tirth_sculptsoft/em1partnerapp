﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Models.Request
{
    public class Request_UpdateQuote : BaseRequestModel
    {
        public string quoteId { get; set; }
        public bool isGiven { get; set; }
        public int status { get; set; }
        public string partnerRemarks { get; set; }
        public int puid { get; set; }
    }
}
