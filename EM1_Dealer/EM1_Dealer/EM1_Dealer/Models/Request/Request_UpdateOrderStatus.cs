﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Models.Request
{
    public class Request_UpdateOrderStatus : BaseRequestModel
    {
        public int orderId { get; set; }
        public int paymentId { get; set; }
        public int statusId { get; set; }
        public string puid { get; set; }
        public double amount { get; set; }
    }
}
