﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Models.Request
{
    public class Request_UpdateTestDrive : BaseRequestModel
    {
            public int testDriveId { get; set; }
            public int status { get; set; }
            public TestDriveTimeSlot testDriveTimeSlot { get; set; }
            public string testDriveDateTime { get; set; }
            public int puid { get; set; }
    }
    public class TestDriveTimeSlot
    {
        public string from { get; set; }
        public string to { get; set; }
    }
}
