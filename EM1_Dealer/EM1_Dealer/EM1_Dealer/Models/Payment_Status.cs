﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Models
{
    public class Payment_Status
    {
        public int PaymentStatusID { get; set; }
        public string PaymentTypeFilterText { get; set; }
        public string PaymentFilter { get; set; }
    }
}
