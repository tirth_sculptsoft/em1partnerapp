﻿using EM1_Dealer.Enums;
using EM1_Dealer.Resources;
using EM1_Dealer.Views;
using EM1_Dealer.Views.Dashboard;
using EM1_Dealer.Views.EM1_Payment;
using EM1_Dealer.Views.EM1_Quotation;
using EM1_Dealer.Views.EM1_TestDrive;
using Plugin.FirebasePushNotification;
using Plugin.FirebasePushNotification.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace EM1_Dealer
{
    public partial class App : Application
    {
        //EM1_Dealer.MainPage mPage;
        public App()
        {
            InitializeComponent();

            //if (Application.Current.Properties.ContainsKey(AppPropertiesKeys.PARTNERUID))
            //{
            //    MainPage = new DashboardHome();
            //}
            //else
            //{
            //    MainPage = new PartnerCodeScreen();
            //}
        }

        protected override void OnStart()
        {
            // Handle when your app starts
            CrossFirebasePushNotification.Current.Subscribe("general");

            if (Application.Current.Properties.ContainsKey(AppPropertiesKeys.PARTNERUID))
            {
                MainPage = new DashboardHome();

                #region FCM PushNotification
                CrossFirebasePushNotification.Current.OnTokenRefresh += (s, p) =>
                {

                    System.Diagnostics.Debug.WriteLine($"TOKEN : {p.Token}");
                };

                CrossFirebasePushNotification.Current.OnNotificationReceived += (s, p) =>
                {
                    System.Diagnostics.Debug.WriteLine("Received");

                };

                CrossFirebasePushNotification.Current.OnNotificationOpened += Current_OnNotificationOpened;
                #region Default NotificationOpened
                //CrossFirebasePushNotification.Current.OnNotificationOpened += (s, p) =>
                //{
                //    System.Diagnostics.Debug.WriteLine("Opened");
                //    foreach (var data in p.Data)
                //    {
                //        System.Diagnostics.Debug.WriteLine($"{data.Key} : {data.Value}");
                //    }

                //    if (!string.IsNullOrEmpty(p.Identifier))
                //    {
                //        System.Diagnostics.Debug.WriteLine($"ActionId: {p.Identifier}");
                //    }

                //};
                #endregion
                #endregion
            }

            else
            {
                MainPage = new PartnerCodeScreen();
            }
        }

        private void Current_OnNotificationOpened(object source, FirebasePushNotificationResponseEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Opened");
            foreach (var data in e.Data)
            {
                System.Diagnostics.Debug.WriteLine($"{data.Key} : {data.Value}");

                #region Notification_Opened Quote
                if (data.Key == AppResources.Noti_Type && (string)data.Value == ((int)GlobalConstants.NotificationTypes.NewQuote).ToString())
                {
                    //Application.Current.MainPage = new NavigationPage(new QuoteRequestScreen());
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        Application.Current.MainPage = new NavigationPage(new QuoteRequestScreen());
                    });
                }
                #endregion

                #region Notification_Opened TestDrive
                else if (data.Key == AppResources.Noti_Type && (string)data.Value == ((int)GlobalConstants.NotificationTypes.NewTestDrive).ToString())
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        Application.Current.MainPage = new NavigationPage(new TestDriveRequestScreen());
                    });
                }
                #endregion

                #region Notification_Opened ManualPayment
                else if (data.Key == AppResources.Noti_Type && (string)data.Value == ((int)GlobalConstants.NotificationTypes.NewPaymentCode).ToString())
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        Application.Current.MainPage = new NavigationPage(new ManualPaymentDetails());
                    });
                }
                #endregion
            }

            if (!string.IsNullOrEmpty(e.Identifier))
            {
                System.Diagnostics.Debug.WriteLine($"ActionId: {e.Identifier}");
            }

            #region Default Plugin Sample
            //else if (e.Data.ContainsKey("type"))
            //{
            //    Device.BeginInvokeOnMainThread(() =>
            //    {
            //        Application.Current.MainPage = new NavigationPage(new QuoteRequestScreen());
            //    });
            //}
            #endregion
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
