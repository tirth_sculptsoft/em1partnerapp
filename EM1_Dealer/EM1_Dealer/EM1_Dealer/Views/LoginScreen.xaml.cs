﻿using EM1_Dealer.Enums;
using EM1_Dealer.Models.Request;
using EM1_Dealer.Request;
using EM1_Dealer.Resources;
using EM1_Dealer.Views.Dashboard;
using Plugin.FirebasePushNotification;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EM1_Dealer.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginScreen : ContentPage
    {
        public LoginScreen()
        {
            InitializeComponent();

            PartnerName.Text = Application.Current.Properties[AppPropertiesKeys.PARTNERNAME].ToString();
            entryusername.Completed += Entryusername_Completed;
        }

        private void Entryusername_Completed(object sender, EventArgs e)
        {
            entrypassword.Focus();
        }

        private async void Login_Clicked(object sender, EventArgs e)
        {
            PartnerUserLoginRequest req = new PartnerUserLoginRequest();

            try
            {
                string token = CrossFirebasePushNotification.Current.Token;
                var res = await req.Execute(new Request_PartnerUserLogin
                {
                    partnerId = (int)Application.Current.Properties[AppPropertiesKeys.PARTNERID],
                    email = entryusername.Text,
                    password = entrypassword.Text,
                    notificationtoken = token,
                    platform = "android"
                });

                if (res != null)
                {
                    if (res.success > 0 && res.data.type == 2)
                    {
                        int puid = res.data.puid;
                        string partneruname = String.Concat(res.data.fname, res.data.lname);
                        Application.Current.Properties.Add(AppPropertiesKeys.PARTNERUID, puid);
                        Application.Current.Properties.Add(AppPropertiesKeys.PARTNERUNAME, partneruname);

                        await Application.Current.SavePropertiesAsync();
                        Application.Current.MainPage = new DashboardHome();
                    }
                    else
                    {
                        await DisplayAlert(AppResources.GlobalAlert_HeaderMessage, AppResources.Wrong_UserPass_Message, AppResources.TryAgain_Button);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(AppResources.ExceptionHeaderMessage + ex.Message);
            }
        }
    }
}