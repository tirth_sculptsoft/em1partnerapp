﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EM1_Dealer.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using EM1_Dealer.ViewModel.TestDrive;
using EM1_Dealer.Enums;
using EM1_Dealer.Request;
using EM1_Dealer.Models.Request;
using System.Globalization;
using EM1_Dealer.Resources;

namespace EM1_Dealer.Views.EM1_TestDrive
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TestDriveRequestDetails : ContentPage
    {
        private TestDriveRequestModel reqitem;
        public TestDriveRequestViewModel TestDriveVM = null;
        public int TestDrive_Status;

        public TestDriveRequestDetails()
        {
            InitializeComponent();
        }

        public TestDriveRequestDetails(TestDriveRequestModel _reqitem)
        {
            InitializeComponent();

            reschedate.MinimumDate = DateTime.Now;
            SubmitTDReqButton.IsEnabled = true;

            TestDriveVM = new TestDriveRequestViewModel();

            foreach (var item in TestDriveVM.TDStatusItems)
            {
                TDStatusPicker.Items.Add(item.TDStatus);
            }

            reqitem = _reqitem;
            BindingContext = reqitem;

            #region TD History
            if (reqitem.TDStatus == "Completed")
            {
                TDStatusPicker.SelectedItem = "Completed";

                DescriptionLabel.IsVisible = false;
                FrameDescription.IsVisible = false;
                SubmitTDReqButton.IsVisible = false;
                TDHistoryStack.IsEnabled = false;
            }
            #endregion

            if (reqitem.TDStatus != null && reqitem.TDStatus != "")
            {
                TDStatusPicker.SelectedItem = reqitem.TDStatus;
            }

            SubmitTDReqButton.Clicked += SubmitTDReqButton_Clicked;
        }

        private async void SubmitTDReqButton_Clicked(object sender, EventArgs e)
        {
            string updatedTDDateTime = reschedate.Date.ToString();
            string fromreshtime = reschefromtime.Time.ToString();
            string toreshtime = reschetotime.Time.ToString();

            if (DescriptionEditor.Text != null && TDStatusPicker.SelectedItem != null)
            {
                var popupres = await DisplayAlert(AppResources.GlobalAlert_HeaderMessage, AppResources.TDPopupMessage, AppResources.Alert_Popup_YesButton, AppResources.Alert_Popup_NoButton);

                if(popupres)
                {
                    SubmitTDReqButton.IsEnabled = false;
                    UpdateTestDriveRequest req = new UpdateTestDriveRequest();

                    //reqTD.testDriveTimeSlot.from = fromreshtime;
                    //reqTD.testDriveTimeSlot.to = toreshtime;

                    #region Put Request For Postponed
                    if ((string)TDStatusPicker.SelectedItem == "Postponed")
                    {
                        await Navigation.PopAsync();

                        //var res = await req.Execute(new Request_UpdateTestDrive
                        //{
                        //    testDriveId = reqitem.TDReqID,
                        //    testDriveDateTime = updatedTDDateTime,
                        //    testDriveTimeSlot = reqTD.testDriveTimeSlot,
                        //    status = TestDrive_Status,
                        //    puid = (int)Application.Current.Properties[AppPropertiesKeys.PARTNERUID]
                        //});

                        //if (res.data.result > 0)
                        //{
                        //    await Navigation.PopAsync();
                        //}

                        //else
                        //{
                        //    await DisplayAlert("Try Again", "Test Drive Not updated !!!", "Ok");
                        //}
                    }
                    #endregion

                    #region Request For Accept-Reject-Complete
                    else
                    {
                        var res = await req.Execute(new Request_UpdateTestDrive
                        {
                            testDriveId = reqitem.TDReqID,
                            status = TestDrive_Status,
                            puid = (int)Application.Current.Properties[AppPropertiesKeys.PARTNERUID]
                        });

                        if (res.data.result > 0)
                        {
                            await Navigation.PopAsync();
                        }

                        else
                        {
                            await DisplayAlert("Try Again", "Test Drive Not updated !!!", "Ok");
                        }
                    }
                    #endregion
                }
            }

            else
            {
                await DisplayAlert("Alert!!", "Please Fill All Required Field", "Okay");
            }
        }

        private void TDStatusPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((string)TDStatusPicker.SelectedItem == "Postponed")
            {
                TestDrive_Status = (int)GlobalConstants.Status.TestDrivePostponed;
                ResheduleStack.IsVisible = true;
            }

            else if ((string)TDStatusPicker.SelectedItem == "Accepted")
            {
                TestDrive_Status = (int)GlobalConstants.Status.TestDriveAccepted;
                ResheduleStack.IsVisible = false;
            }

            else if ((string)TDStatusPicker.SelectedItem == "Rejected")
            {
                TestDrive_Status = (int)GlobalConstants.Status.TestDriveAccepted;
                ResheduleStack.IsVisible = false;
            }

            else if ((string)TDStatusPicker.SelectedItem == "Completed")
            {
                TestDrive_Status = (int)GlobalConstants.Status.TestDriveCompleted;
                ResheduleStack.IsVisible = false;
            }
        }
    }
}