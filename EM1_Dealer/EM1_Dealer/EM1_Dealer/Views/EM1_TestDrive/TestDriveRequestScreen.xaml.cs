﻿using EM1_Dealer.Models;
using EM1_Dealer.Resources;
using EM1_Dealer.ViewModel.TestDrive;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EM1_Dealer.Views.EM1_TestDrive
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TestDriveRequestScreen : ContentPage
    {
        public ListView TestDriveRequestListView;
        TestDriveRequestViewModel TestDriveReqVM = null;

        public TestDriveRequestScreen()
        {
            InitializeComponent();

            TestDriveReqVM = new TestDriveRequestViewModel();
            TestDriveReqVM.NoDataEvent += NoDataHandler;

            BindingContext = TestDriveReqVM;

            //TDStatusPicker.SelectedIndex = 0;
            TestDriveRequestListView = TestDriveReqList;

            TestDriveReqList.ItemSelected += TestDriveReqList_ItemSelected;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            TestDriveReqVM.LoadData();
        }

        public async void NoDataHandler(object sender,string e)
        {
            await DisplayAlert(AppResources.Alert_NoPending_Title, AppResources.Alert_NoPendingTestDrive, AppResources.OkButtonText);
            await Navigation.PopToRootAsync();
        }

        private async void TestDriveReqList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if(e.SelectedItem!=null)
            {
                TestDriveRequestModel reqitem = (TestDriveRequestModel)e.SelectedItem;
                await Navigation.PushAsync(new TestDriveRequestDetails(reqitem));
                TestDriveReqList.SelectedItem = null;
            }
        }
    }
}