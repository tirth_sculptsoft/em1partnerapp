﻿using EM1_Dealer.Models;
using EM1_Dealer.ViewModel.TestDrive;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EM1_Dealer.Views.EM1_TestDrive
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TestDriveHistoryScreen : ContentPage
    {
        public ListView TDHistoryListView;
        public TestDriveRequestViewModel TestDriveVM = null;
        public TestDriveHistoryScreen()
        {
            InitializeComponent();

            TestDriveVM = new TestDriveRequestViewModel();
            BindingContext = TestDriveVM;

            TDHistoryListView = TestDriveHistoryList;
            TestDriveHistoryList.ItemSelected += TestDriveHistoryList_ItemSelected;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            TestDriveVM.LoadHistoryData();
        }

        private async void TestDriveHistoryList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if(e.SelectedItem!=null)
            {
                TestDriveRequestModel historyitem = (TestDriveRequestModel)e.SelectedItem;

                await Navigation.PushAsync(new TestDriveRequestDetails(historyitem)); 
                TestDriveHistoryList.SelectedItem = null;
            }
        }
    }
}