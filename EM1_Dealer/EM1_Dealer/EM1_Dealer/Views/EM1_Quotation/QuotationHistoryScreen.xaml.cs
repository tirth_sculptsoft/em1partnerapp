﻿using EM1_Dealer.Models;
using EM1_Dealer.ViewModel.Quotation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EM1_Dealer.Views.EM1_Quotation
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class QuotationHistoryScreen : ContentPage
    {
        public ListView QuotationHistoryListView;
        public QuotationRequestViewModel QuoteReqVM = null;
        public QuotationHistoryScreen()
        {
            InitializeComponent();

            QuoteReqVM = new QuotationRequestViewModel();
            BindingContext = QuoteReqVM;
            QuotationHistoryListView = QuoteHistoryList;

            QuoteHistoryList.ItemSelected += QuoteHistoryList_ItemSelected;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            QuoteReqVM.LoadHistoryData();
        }

        private async void QuoteHistoryList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if(e.SelectedItem!=null)
            {
                QuotationRequestModel reqitem = (QuotationRequestModel)e.SelectedItem;
                await Navigation.PushAsync(new QuoteRequestDetails(reqitem));
                QuoteHistoryList.SelectedItem = null;
            }
        }
    }
}