﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EM1_Dealer.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using EM1_Dealer.Request;
using EM1_Dealer.Enums;
using EM1_Dealer.Models.Request;
using EM1_Dealer.Resources;
using System.Diagnostics;

namespace EM1_Dealer.Views.EM1_Quotation
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class QuoteRequestDetails : ContentPage
    {
        private QuotationRequestModel reqitem;
        bool IsGiven;

        public QuoteRequestDetails()
        {
            InitializeComponent();
        }

        public QuoteRequestDetails(QuotationRequestModel _reqitem)
        {
            InitializeComponent();

            IsGiven = false;
            reqitem = _reqitem;

            #region QuoteHistory Portion
            if (reqitem.QuoteClosedStatus == "Closed")
            {
                HistoryStack.IsEnabled = false;
                ClosedButton.IsVisible = false;

                remarkseditor.Text = reqitem.PartnerRemarks;

                if (reqitem.QuoteStatus == "Quote is given")
                {
                    QuoteSwitch.IsToggled = true;
                }
                else if (reqitem.QuoteStatus == "Quote is not given")
                {
                    QuoteSwitch.IsToggled = false;
                }
            }
            #endregion

            BindingContext = reqitem;

            ClosedButton.Clicked += ClosedButton_Clicked;

        }

        private async void ClosedButton_Clicked(object sender, EventArgs e)
        {
            if (remarkseditor.Text != null)
            {
                var popupres = await DisplayAlert(AppResources.GlobalAlert_HeaderMessage, AppResources.QuotePopupMessage, AppResources.Alert_Popup_YesButton, AppResources.Alert_Popup_NoButton);

                if(popupres)
                {
                    try
                    {
                        UpdateQuoteRequest req = new UpdateQuoteRequest();

                        var res = await req.Execute(new Request_UpdateQuote
                        {
                            quoteId = reqitem.QuoteID,
                            isGiven = IsGiven,
                            partnerRemarks = remarkseditor.Text,
                            puid = (int)Application.Current.Properties[AppPropertiesKeys.PARTNERUID],
                            status = ((int)GlobalConstants.Status.QuoteClosed)
                        });

                        if (res.data.result > 0)
                        {
                            await Navigation.PopAsync();
                        }

                        else
                        {
                            await DisplayAlert("Try Again", "Quote Not updated !!!", "Ok");
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(AppResources.ExceptionHeaderMessage + ex.Message);
                    }
                }
            }

            else
            {
                await DisplayAlert("Alert!!", "Please Fill All Required Field", "Okay");
            }
        }

        private void QuoteSwitch_Toggled(object sender, ToggledEventArgs e)
        {
            if (QuoteSwitch.IsToggled)
            {
                IsGiven = true;
            }

            else if (!QuoteSwitch.IsToggled)
            {
                IsGiven = false;
            }
        }
    }
}