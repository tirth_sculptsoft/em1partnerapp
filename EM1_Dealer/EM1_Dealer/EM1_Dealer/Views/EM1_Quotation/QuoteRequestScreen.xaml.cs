﻿using EM1_Dealer.Models;
using EM1_Dealer.Resources;
using EM1_Dealer.ViewModel.Quotation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EM1_Dealer.Views.EM1_Quotation
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class QuoteRequestScreen : ContentPage
    {
        public ListView QuotationRequestListView;
        public QuotationRequestViewModel QuoteReqVm = null;
        public QuoteRequestScreen()
        {
            InitializeComponent();

            QuoteReqVm = new QuotationRequestViewModel();
            QuoteReqVm.NoDataEvent += NoDataHandler;

            BindingContext = QuoteReqVm;
            QuotationRequestListView = QuoteReqList;

            QuoteReqList.ItemSelected += QuoteReqList_ItemSelected;
        }

        private async void NoDataHandler(object sender, string e)
        {
            await DisplayAlert(AppResources.Alert_NoPending_Title,AppResources.Alert_NoPendingQuote,AppResources.OkButtonText);
            await Navigation.PopToRootAsync(); 
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            QuoteReqVm.LoadData();
        }

        private async void QuoteReqList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if(e.SelectedItem!=null)
            {
                QuotationRequestModel reqitem = (QuotationRequestModel)e.SelectedItem;
                await Navigation.PushAsync(new QuoteRequestDetails(reqitem));
                QuoteReqList.SelectedItem = null;
            }
        }
    }
}