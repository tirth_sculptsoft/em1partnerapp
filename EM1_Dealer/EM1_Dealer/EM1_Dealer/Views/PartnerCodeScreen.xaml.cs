﻿using EM1_Dealer.Enums;
using EM1_Dealer.Request;
using EM1_Dealer.Resources;
using EM1_Dealer.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EM1_Dealer.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PartnerCodeScreen : ContentPage
    {
        public PartnerCodeScreen()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ConfirmButton.IsEnabled = true;
        }

        private async void Confirm_Clicked(object sender, EventArgs e)
        {
            Application.Current.Properties.Clear();
            await Application.Current.SavePropertiesAsync();

            ConfirmButton.IsEnabled = false;
            GetPartnerByCodeRequest req = new GetPartnerByCodeRequest();

            try
            {
                var res = await req.Execute(codeentry.Text);

                if (res != null)
                {
                    if (res.data.list.Count > 0)
                    {
                        int PID = res.data.list.FirstOrDefault().pid;
                        string PartnerName = res.data.list.FirstOrDefault().name;

                        Application.Current.Properties.Add(AppPropertiesKeys.PARTNERID, PID);
                        Application.Current.Properties.Add(AppPropertiesKeys.PARTNERNAME, PartnerName);
                        await Application.Current.SavePropertiesAsync();

                        await Navigation.PushModalAsync(new LoginScreen());
                    }
                    else
                    {
                        ConfirmButton.IsEnabled = true;
                        await DisplayAlert(AppResources.GlobalAlert_HeaderMessage, AppResources.WrongPartnerCode_Message, AppResources.TryAgain_Button);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(AppResources.ExceptionHeaderMessage + ex.Message);
            }
        }
    }
}