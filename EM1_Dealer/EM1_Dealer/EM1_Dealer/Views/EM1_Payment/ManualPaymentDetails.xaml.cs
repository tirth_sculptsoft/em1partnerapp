﻿using EM1_Dealer.Enums;
using EM1_Dealer.Models;
using EM1_Dealer.Models.Request;
using EM1_Dealer.Request;
using EM1_Dealer.Resources;
using EM1_Dealer.ViewModel.Payment;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EM1_Dealer.Views.EM1_Payment
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ManualPaymentDetails : ContentPage
    {
        //List<PaymentRequestModel> paymentrequestitems;
        PaymentRequestViewModel PaymentRequestVM = new PaymentRequestViewModel();

        PaymentRequestModel paymentreqmodel = null;
        public ManualPaymentDetails()
        {
            InitializeComponent();

            entryforcode.Keyboard = Keyboard.Numeric;
            entryfordealamount.Keyboard = Keyboard.Numeric;
            //VerfiyButton.Text = "Verify Code";
            paymentdetailstack.IsVisible = false;

            AcceptButton.Clicked += AcceptButton_Clicked;
            RejectButton.Clicked += RejectButton_Clicked;
        }

        private async void VerifyButton_Clicked(object sender, EventArgs e)
        {
            bool IsFound = true;
            //paymentrequestitems = PaymentRequestVM.paymentreqitems.Where(m => m.PaymentType == "Manual").ToList();

            paymentreqmodel = new PaymentRequestModel();
            GetOrderByPaymentCodeRequest req = new GetOrderByPaymentCodeRequest();

            try
            {
                var res = await req.Execute(entryforcode.Text);

                if (res != null)
                {
                    if (res.data.list.Count > 0)
                    {
                        AcceptButton.IsEnabled = true;
                        paymentdetailstack.IsVisible = true;
                        ResButtonStack.IsVisible = true;
                        VerfiyButton.Text = "Verified Code";

                        foreach (var orderitem in res.data.list)
                        {
                            paymentreqmodel.CustomerName = orderitem.fullname;

                            #region VehicleImage
                            if (orderitem.prodCat == VehicleCategoryConstants.NEWFOURWHEEL)
                            {
                                paymentreqmodel.PaymentReqImage = VehicleImageConstants.FOURWHEELIMAGE;
                            }
                            else if (orderitem.prodCat == VehicleCategoryConstants.NEWTWOWHEEL)
                            {
                                paymentreqmodel.PaymentReqImage = VehicleImageConstants.TWOWHEELIMAGE;
                            }
                            #endregion

                            #region Deal_Amount Entry
                            if (orderitem.isamontneeded)
                            {
                                entryfordealamount.IsEnabled = true;
                            }
                            else
                            {
                                entryfordealamount.IsEnabled = false;
                            }
                            #endregion

                            paymentreqmodel.OrderID = (int)orderitem.orderId;
                            paymentreqmodel.PaymentID = (int)orderitem.paymentId;
                            paymentreqmodel.VehicleModelName = orderitem.prodBrand;
                            paymentreqmodel.ModelColor = orderitem.prodColor;
                            paymentreqmodel.PaymentAmount = "\u20B9 " + orderitem.paymentamout.ToString();
                        }

                        BindingContext = paymentreqmodel;
                        IsFound = true;
                    }

                    else
                    {
                        IsFound = false;
                    }
                }
            }

            catch (Exception ex)
            {
                Debug.WriteLine(AppResources.ExceptionHeaderMessage + ex.Message);
            }

            #region Commented
            //foreach (var payitem in paymentrequestitems)
            //{
            //    if (entryforcode.Text == payitem.PaymentCode)
            //    {
            //        paymentdetailstack.IsVisible = true;
            //        ResButtonStack.IsVisible = true;
            //        VerfiyButton.Text = "Verified Code";

            //        BindingContext = payitem;
            //        IsFound = true;
            //        break;
            //    }

            //    else
            //    {
            //        IsFound = false;
            //    }
            //}
            #endregion

            if (!IsFound)
            {
                ResButtonStack.IsVisible = false;
                paymentdetailstack.IsVisible = false;
                VerfiyButton.Text = "Verify Code";
                await DisplayAlert("Oopss!!", "Sorry You've enterted wrong payment code !!", "Try Again!!");
            }

        }

        private async void AcceptButton_Clicked(object sender, EventArgs e)
        {
            UpdateOrderStatusRequest req = new UpdateOrderStatusRequest();

            try
            {
                var popupres = await DisplayAlert(AppResources.GlobalAlert_HeaderMessage, AppResources.Payment_AcceptPopupMessage, AppResources.Alert_Popup_YesButton, AppResources.Alert_Popup_NoButton);

                if (popupres)
                {
                    AcceptButton.IsEnabled = false;
                    var res = await req.Execute(new Request_UpdateOrderStatus
                    {
                        orderId = paymentreqmodel.OrderID,
                        paymentId = paymentreqmodel.PaymentID,
                        puid = Application.Current.Properties[AppPropertiesKeys.PARTNERUID].ToString(),
                        statusId = ((int)GlobalConstants.Status.OrderPaymentConfirmed),
                        amount = Convert.ToDouble(entryfordealamount.Text)
                    });

                    if (res != null)
                    {
                        if (res.data.result > 0)
                        {
                            ResButtonStack.IsVisible = false;
                            paymentdetailstack.IsVisible = false;
                            VerfiyButton.Text = "Verify Code";

                            entryforcode.Text = "";
                            entryfordealamount.Text = "";
                        }
                        else
                        {
                            await DisplayAlert("Oopss!!", "Order not updated!!", "Try Again!!");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(AppResources.ExceptionHeaderMessage + ex.Message);
            }
        }
        private async void RejectButton_Clicked(object sender, EventArgs e)
        {
            UpdateOrderStatusRequest req = new UpdateOrderStatusRequest();

            try
            {
                var popupres = await DisplayAlert(AppResources.GlobalAlert_HeaderMessage, AppResources.Payment_RejectPopupMessage, AppResources.Alert_Popup_YesButton, AppResources.Alert_Popup_NoButton);

                if (popupres)
                {
                    var res = await req.Execute(new Request_UpdateOrderStatus
                    {
                        orderId = paymentreqmodel.OrderID,
                        paymentId = paymentreqmodel.PaymentID,
                        puid = Application.Current.Properties[AppPropertiesKeys.PARTNERUID].ToString(),
                        statusId = ((int)GlobalConstants.Status.OrderPaymentDeclined)
                    });

                    if (res != null)
                    {
                        if (res.data.result > 0)
                        {
                            ResButtonStack.IsVisible = false;
                            paymentdetailstack.IsVisible = false;
                            VerfiyButton.Text = "Verify Code";

                            entryforcode.Text = "";
                        }
                        else
                        {
                            await DisplayAlert("Oopss!!", "Order not updated!!", "Try Again!!");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(AppResources.ExceptionHeaderMessage + ex.Message);
            }
        }
    }
}