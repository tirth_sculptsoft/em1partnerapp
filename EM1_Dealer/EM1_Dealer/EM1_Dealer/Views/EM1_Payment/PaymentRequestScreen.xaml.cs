﻿using EM1_Dealer.Models;
using EM1_Dealer.Helpers;
using EM1_Dealer.ViewModel.Payment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Diagnostics;

namespace EM1_Dealer.Views.EM1_Payment
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PaymentRequestScreen : ContentPage
    {
        public ListView PaymentReqListView;
        public PaymentRequestViewModel PaymentReqVM;
        public PaymentRequestScreen()
        {
            InitializeComponent();
            PaymentReqVM = new PaymentRequestViewModel();

            BindingContext = PaymentReqVM;

            PaymentReqListView = PaymentList;
            PaymentList.ItemSelected += PaymentReqList_ItemSelected;

            TDStatusPicker.SelectedIndexChanged += TDStatusPicker_SelectedIndexChanged;
        }

        private void TDStatusPicker_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private async void PaymentReqList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if(e.SelectedItem!=null)
            {
                PaymentRequestModel item = (PaymentRequestModel)e.SelectedItem;

                await Navigation.PushAsync(new PaymentRequestDetails(item));
                PaymentList.SelectedItem = null;
            }
        }

        private async void ManualPayment_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ManualPaymentDetails());
        }
    }
}