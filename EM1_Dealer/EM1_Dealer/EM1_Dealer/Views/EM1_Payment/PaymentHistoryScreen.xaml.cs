﻿using EM1_Dealer.Models;
using EM1_Dealer.ViewModel.Payment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EM1_Dealer.Views.EM1_Payment
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PaymentHistoryScreen : ContentPage
    {
        public ListView PaymentHistoryListView;
        public PaymentHistoryScreen()
        {
            InitializeComponent();

            BindingContext = new PaymentRequestViewModel();
            PaymentHistoryListView = PaymentHistoryList;

            PaymentHistoryList.ItemSelected += PaymentHistoryList_ItemSelected;
        }

        private async void PaymentHistoryList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {
                PaymentRequestModel paymenthistoryitems = (PaymentRequestModel)e.SelectedItem;

                await Navigation.PushAsync(new PaymentRequestDetails(paymenthistoryitems));
                PaymentHistoryList.SelectedItem = null;
            }
        }
    }
}