﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EM1_Dealer.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EM1_Dealer.Views.EM1_Payment
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PaymentRequestDetails : ContentPage
    {
        private PaymentRequestModel item;

        public PaymentRequestDetails()
        {
            InitializeComponent();
        }

        public PaymentRequestDetails(PaymentRequestModel _item)
        {
            InitializeComponent();

            item = _item;

            BindingContext = item;
        }
    }
}