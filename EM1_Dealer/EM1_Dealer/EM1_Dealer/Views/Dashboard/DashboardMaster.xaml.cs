﻿using EM1_Dealer.Enums;
using EM1_Dealer.Resources;
using EM1_Dealer.ViewModel.Dashboard;
using EM1_Dealer.Views.EM1_Quotation;
using Plugin.FirebasePushNotification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EM1_Dealer.Views.Dashboard
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DashboardMaster : ContentPage
    {
        public ListView MenuListView;
        public DashboardMaster()
        {
            InitializeComponent();

            partnername.Text = Application.Current.Properties[AppPropertiesKeys.PARTNERNAME].ToString();
            partnerusername.Text = Application.Current.Properties[AppPropertiesKeys.PARTNERUNAME].ToString();
            BindingContext = new DashboardMasterMenuViewModel();
            MenuListView = DashboardMenuList;

            LogoutButton.Clicked += LogoutButton_Clicked;
        }

        private async void LogoutButton_Clicked(object sender, EventArgs e)
        {
            var popupres = await DisplayAlert(AppResources.GlobalAlert_HeaderMessage, AppResources.UserLogout_Message, AppResources.Alert_Popup_YesButton, AppResources.Alert_Popup_NoButton);

            if (popupres)
            {
                Application.Current.Properties.Clear();
                await Application.Current.SavePropertiesAsync();

                Application.Current.MainPage = new PartnerCodeScreen();
            }
        }
    }
}