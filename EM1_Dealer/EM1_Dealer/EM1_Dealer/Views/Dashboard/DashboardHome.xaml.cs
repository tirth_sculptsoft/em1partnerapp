﻿using EM1_Dealer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EM1_Dealer.Views.Dashboard
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DashboardHome : MasterDetailPage
    {
        public DashboardHome()
        {
            InitializeComponent();

            DashboardMaster.MenuListView.ItemSelected += MenuListView_ItemSelected;
        }

        private void MenuListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as DashboardMasterMenuItem;

            if (item == null)
                return;

            var page = (Page)Activator.CreateInstance(item.TargetType);
            Detail = new NavigationPage(page);
            
            IsPresented = false;

            DashboardMaster.MenuListView.SelectedItem = null;
        }
    }
}