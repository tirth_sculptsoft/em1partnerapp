﻿using EM1_Dealer.Models;
using EM1_Dealer.ViewModel.Dashboard;
using EM1_Dealer.Views.EM1_Payment;
using EM1_Dealer.Views.EM1_Quotation;
using EM1_Dealer.Views.EM1_TestDrive;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EM1_Dealer.Views.Dashboard
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DashboardDetail : ContentPage
    {
        public ListView PendingItemListView;
        public DashboardPendingViewModel DashPendingVM = null;
        public DashboardDetail()
        {
            InitializeComponent();

            DashPendingVM = new DashboardPendingViewModel();
            BindingContext = DashPendingVM;
            PendingItemListView = DashboardPendingList;

            DashboardPendingList.ItemSelected += DashboardPendingList_ItemSelected;
        }

        private async void DashboardPendingList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if(e.SelectedItem!=null)
            {
                DashboardPendingItem DBPendingitem = (DashboardPendingItem)e.SelectedItem;

                if(DBPendingitem.Id==1)
                {
                    await Navigation.PushAsync(new QuoteRequestScreen());
                    DashboardPendingList.SelectedItem = null;
                }

                else if (DBPendingitem.Id == 2)
                {
                    await Navigation.PushAsync(new TestDriveRequestScreen());
                    DashboardPendingList.SelectedItem = null;
                }

                else if (DBPendingitem.Id == 3)
                {
                    await Navigation.PushAsync(new PaymentRequestScreen());
                    DashboardPendingList.SelectedItem = null;
                }

                else if (DBPendingitem.Id == 4)
                {
                    //await Navigation.PushAsync(new TestDriveRequestScreen());
                    DashboardPendingList.SelectedItem = null;
                }
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            DashPendingVM.LoadData();
        }

    }
}