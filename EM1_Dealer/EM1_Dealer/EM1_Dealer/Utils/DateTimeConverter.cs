﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Utils
{
    public class DateTimeConverter
    {
        public const string FromDTFormat = "M/dd/yyyy HH:mm:ss tt";
        public const string ToDTFormat = "dd-MMM-yy hh:mmtt";
        public const string FromTimeFormat = "HH:mm:ss.fff";
        public const string ToTimeFormat = "hh:mmtt";

        public static string TimeSlotConvert(string fromtime,string totime)
        {
            string fromconverttime= DateTime.ParseExact(fromtime, FromTimeFormat, CultureInfo.InvariantCulture).ToString(ToTimeFormat);
            string toconverttime = DateTime.ParseExact(totime, FromTimeFormat, CultureInfo.InvariantCulture).ToString(ToTimeFormat);
            return String.Concat(fromconverttime, " to ",toconverttime);
        }

        public static string DateTimeFormatConvert(string createdDate)
        {
            return DateTime.ParseExact(createdDate,FromDTFormat, CultureInfo.InvariantCulture).ToString(ToDTFormat);
        }
    }
}
