﻿using EM1_Dealer.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EM1_Dealer.Utils
{
    public class UrlGenerator
    {
        //local URL
        static string baseURL = "http://192.168.1.113/EM1API/api";

        //AWS URL
        //static string baseURL = "http://ec2-35-154-180-152.ap-south-1.compute.amazonaws.com/api";

        #region OLD AWS URL
        //static string baseURL = "http://ec2-13-126-195-41.ap-south-1.compute.amazonaws.com/api";
        #endregion

        //static string partnerId = "1";
        //  public const  string partnerId = Application.Current.Properties["PID"].ToString();

        #region QuoteStatus
        static string pendingstatus = ((int)GlobalConstants.Status.QuotePending).ToString();
        static string closedstatus = ((int)GlobalConstants.Status.QuoteClosed).ToString();
        #endregion
        #region TestDrive Status
        static string TDpendingstatus = ((int)GlobalConstants.Status.TestDrivePending).ToString();
        static string TDcompletedstatus = ((int)GlobalConstants.Status.TestDriveCompleted).ToString();
        #endregion
        #region Payment Status
        static string OrderAcceptPaymentStatus = ((int)GlobalConstants.Status.OrderPaymentConfirmed).ToString();
        static string OrderRejectPaymentStatus = ((int)GlobalConstants.Status.OrderPaymentDeclined).ToString();
        #endregion

        public static string GetPartnerByCodeURL(string partnercode)
        {
            return string.Concat(baseURL, "/Partner/GetPartnerByCode/", partnercode);
        }
        public static string PartnerUserLoginURL()
        {
            return string.Concat(baseURL, "/PartnerUser/Login");
        }

        public static string PartnerDashboardURL()
        {
            string partnerId = Application.Current.Properties[AppPropertiesKeys.PARTNERID].ToString();
            return string.Concat(baseURL, "/PartnerDashboard/", partnerId);
        }

        public static string GetReqQuoteURL()
        {
            string partnerId = Application.Current.Properties[AppPropertiesKeys.PARTNERID].ToString();
            return string.Concat(baseURL, "/PartnerQuote/Get?partnerId=", partnerId, "&status=", pendingstatus);
        }
        public static string GetQuoteHistoryURL()
        {
            string partnerId = Application.Current.Properties[AppPropertiesKeys.PARTNERID].ToString();
            return string.Concat(baseURL, "/PartnerQuote/Get?partnerId=", partnerId, "&status=", closedstatus);
        }
        public static string UpdateQuoteURL()
        {
            return string.Concat(baseURL, "/PartnerQuote");
        }

        public static string GetReqTestDriveURL()
        {
            string partnerId = Application.Current.Properties[AppPropertiesKeys.PARTNERID].ToString();
            return string.Concat(baseURL, "/PartnerTestDrive/Get?partnerId=", partnerId, "&status=", TDpendingstatus);
        }
        public static string GetTestDriveHistoryURL()
        {
            string partnerId = Application.Current.Properties[AppPropertiesKeys.PARTNERID].ToString();
            return string.Concat(baseURL, "/PartnerTestDrive/Get?partnerId=",partnerId,"&status=",TDcompletedstatus);
        }
        public static string UpdateTestDriveURL()
        {
            return string.Concat(baseURL, "/PartnerTestDrive");
        }


        public static string GetOrderByPaymentCodeURL(string paymentcode)
        {
            string partnerId = Application.Current.Properties[AppPropertiesKeys.PARTNERID].ToString();
            return string.Concat(baseURL, "/PartnerPayment/GetOrderByPaymentCode/", paymentcode, "/", partnerId);
        }
        public static string UpdateOrderStatusURL()
        {
            return string.Concat(baseURL, "/Order/UpdateOrderStatus");
        }
    }
}
