﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.Enums
{
    public static class GlobalConstants
    {
        public enum StatusTypes
        {
            QuoteStatus = 1,
            TestDriveStatus = 2
        }

        public enum Status
        {
            QuotePending = 1,
            QuoteClosed = 2,
            TestDrivePending = 3,
            TestDriveAccepted = 4,
            TestDriveRejected = 5,
            TestDriveCompleted = 6,
            TestDrivePostponed = 13,
            OrderPaymentConfirmed = 16,
            OrderPaymentDeclined = 17
        }

        public enum NotificationTypes
        {
            NewQuote = 1,
            NewTestDrive = 3,
            NewPaymentCode = 5
        }
    }
    public static class AppPropertiesKeys
    {
        public const string PARTNERID = "PID";
        public const string PARTNERUID = "puid";
        public const string PARTNERNAME = "PNAME";
        public const string PARTNERUNAME = "PUNAME";
    }
    public static class VehicleCategoryConstants
    {
        public const string NEWFOURWHEEL = "new-four-wheelers";
        public const string NEWTWOWHEEL = "new-two-wheelers";
    }
    public static class VehicleImageConstants
    {
        public const string FOURWHEELIMAGE = "car_gray.png";
        public const string TWOWHEELIMAGE = "scooter_gray.png";
    }
}
