﻿using EM1_Dealer.Enums;
using EM1_Dealer.Helpers;
using EM1_Dealer.Models;
using EM1_Dealer.Request;
using EM1_Dealer.Resources;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace EM1_Dealer.ViewModel.Dashboard
{
    public class DashboardPendingViewModel : BaseViewModel
    {
        public ObservableRangeCollection<DashboardPendingItem> dashboardpendingitems { get; set; }
        List<DashboardPendingItem> dblist = null;

        bool _IsRefreshing;
        public bool IsRefreshing
        {
            get
            {
                return _IsRefreshing;
            }
            set
            {
                SetProperty(ref _IsRefreshing, value);
            }
        }

        public ICommand RefreshCommand { protected set; get; }

        public DashboardPendingViewModel()
        {
            dashboardpendingitems = new ObservableRangeCollection<DashboardPendingItem>();

            this.RefreshCommand = new Command(() =>
            {
                IsRefreshing = true;
                LoadData();
            });
        }

        public async void LoadData()
        {
            try
            {
                IsBusy = true;

                PartnerDashboardRequest req = new PartnerDashboardRequest();
                //GetPendingQuoteRequest req = new GetPendingQuoteRequest();

                var res = await req.Execute();

                if (res != null)
                {
                    if (res.success > 0)
                    {
                        #region Static Data
                        dblist = new List<DashboardPendingItem>(new[]
                        {
                            new DashboardPendingItem { Id=1,listbgcolor="#E7AD3D",pendingcount=res.data.quoteCount,pendingtext="Pending Quotes",pendingtextdesc="Response to a request for quotation" },
                            new DashboardPendingItem { Id=2,listbgcolor="#0D5D6D",pendingcount=res.data.testDriveCount,pendingtext="Pending Test Drives",pendingtextdesc="Test drive from home" },
                            new DashboardPendingItem { Id=3,listbgcolor="#78C848",pendingcount=res.data.paymentCount,pendingtext="Pending Payments",pendingtextdesc="Discover new way to pay" },
                            new DashboardPendingItem { Id=4,listbgcolor="#00B0EF",pendingcount=0,pendingtext="Pending Services",pendingtextdesc="Let us take care of your vehicle" },
                        });
                        #endregion
                    }
                }

                dashboardpendingitems.ReplaceRange(dblist);

                IsBusy = false;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(AppResources.ExceptionHeaderMessage + ex.Message);
            }

            finally
            {
                Debug.WriteLine("In Finally");
                IsRefreshing = false;
            }
        }
    }
}
