﻿using EM1_Dealer.Models;
using EM1_Dealer.Views.Dashboard;
using EM1_Dealer.Views.EM1_Payment;
using EM1_Dealer.Views.EM1_Quotation;
using EM1_Dealer.Views.EM1_TestDrive;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.ViewModel.Dashboard
{
    public class DashboardMasterMenuViewModel : BaseViewModel
    {
        public ObservableCollection<DashboardMasterMenuItem> dashboardmenuitems { get; set; }

        public DashboardMasterMenuViewModel()
        {
            dashboardmenuitems = new ObservableCollection<DashboardMasterMenuItem>(new[]
            {
                new DashboardMasterMenuItem { Id=0,Title="EM1 Dashboard",TargetType=typeof(DashboardDetail) },
                new DashboardMasterMenuItem { Id=1,Title="Quotation History",TargetType=typeof(QuotationHistoryScreen) },
                new DashboardMasterMenuItem { Id=2,Title="Test Drive History",TargetType=typeof(TestDriveHistoryScreen)},
                new DashboardMasterMenuItem { Id=3,Title="Payment History",TargetType=typeof(PaymentHistoryScreen)},
                new DashboardMasterMenuItem { Id=4,Title="Service History",TargetType=typeof(DashboardDetail)},
            });
        }
    }
}
