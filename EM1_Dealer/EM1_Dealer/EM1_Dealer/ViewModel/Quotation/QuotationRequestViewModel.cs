﻿using EM1_Dealer.Enums;
using EM1_Dealer.Helpers;
using EM1_Dealer.Models;
using EM1_Dealer.Request;
using EM1_Dealer.Resources;
using EM1_Dealer.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace EM1_Dealer.ViewModel.Quotation
{
    public class QuotationRequestViewModel : BaseViewModel
    {
        public QuotationRequestModel quotemodel = null;
        public EventHandler<string> NoDataEvent;
        public ObservableRangeCollection<QuotationRequestModel> quotereqitems { get; set; }

        bool _IsRefreshing;

        public bool IsRefreshing
        {
            get
            {
                return _IsRefreshing;
            }
            set
            {
                SetProperty(ref _IsRefreshing, value);
            }
        }

        public ICommand RefreshCommand { protected set; get; }
        public ICommand RefreshHistoryCommand { protected set; get; }

        public QuotationRequestViewModel()
        {
            quotereqitems = new ObservableRangeCollection<QuotationRequestModel>();

            this.RefreshCommand = new Command(() =>
            {
                IsBusy = true;
                LoadData();
            });

            this.RefreshHistoryCommand = new Command(() =>
            {
                IsBusy = true;
                LoadHistoryData();
            });

            #region Static Data
            //quotereqitems = new ObservableCollection<QuotationRequestModel>(new[]
            //{
            //    new QuotationRequestModel { QID=1,CustomerName="Customer 1",QuoteReqDateTime="01/14/2018 08:56AM",QuoteReqImage="car_gray.png",QuoteStatus="Quote is given",StatusColor="#11A442",VehicleModelName="Mercedes-Benz GLC-Class 2016",VehicleVersionName="220D 4MATIC STYLE",ModelColor="Iridium Silver" },
            //    new QuotationRequestModel { QID=2,CustomerName="Customer 2",QuoteReqDateTime="01/15/2018 15:34PM",QuoteReqImage="car_gray.png",QuoteStatus="Quote is given",StatusColor="#11A442",VehicleModelName="Maruti Swift 2015",VehicleVersionName="Vxi Petrol",ModelColor="Polar White" },
            //    new QuotationRequestModel { QID=3,CustomerName="Customer 3",QuoteReqDateTime="01/17/2018 07:48AM",QuoteReqImage="scooter_gray.png",QuoteStatus="Quote is not given",StatusColor="#FF0000",VehicleModelName="Honda Activa 125 2013",VehicleVersionName="3G 125cc",ModelColor="Cherry Red" },
            //    new QuotationRequestModel { QID=4,CustomerName="Customer 4",QuoteReqDateTime="01/17/2018 18:16PM",QuoteReqImage="car_gray.png",QuoteStatus="Quote is given",StatusColor="#11A442",VehicleModelName="Royal Enfield Classic 350 2016",VehicleVersionName="350cc BSIV",ModelColor="Diamond Silver Metalic" },
            //});
            #endregion
        }

        public async void LoadData()
        {
            GetPendingQuoteRequest req = new GetPendingQuoteRequest();

            try
            {
                IsBusy = true;
                var res = await req.Execute();

                List<QuotationRequestModel> quotelist = new List<QuotationRequestModel>();

                if (res != null)
                {
                    if (res.data.list.Count > 0)
                    {
                        quotemodel = new QuotationRequestModel();
                        foreach (var quoteitem in res.data.list)
                        {
                            string tempdate = quoteitem.createdDate.ToString();

                            if (quoteitem.categoryCode == VehicleCategoryConstants.NEWFOURWHEEL)
                            {
                                quotemodel.QuoteReqImage = VehicleImageConstants.FOURWHEELIMAGE;
                            }
                            else if (quoteitem.categoryCode == VehicleCategoryConstants.NEWTWOWHEEL)
                            {
                                quotemodel.QuoteReqImage = VehicleImageConstants.TWOWHEELIMAGE;
                            }

                            quotelist.Add(new QuotationRequestModel
                            {
                                QuoteID = quoteitem.quoteId.ToString(),
                                CustomerName = quoteitem.fullname,
                                QuoteReqDateTime = quoteitem.createdDate.ToString(),
                                PreferTimeSlot = DateTimeConverter.TimeSlotConvert(quoteitem.timeSlot.from, quoteitem.timeSlot.to),
                                QuoteStatus = quoteitem.status,
                                QuoteReqImage = quotemodel.QuoteReqImage,
                                VehicleModelName = quoteitem.productName,
                                ModelColor = quoteitem.varientColor,
                            });
                        }
                        quotereqitems.ReplaceRange(quotelist);
                    }
                    else
                    {
                        NoDataEvent?.Invoke(this, null);
                    }
                }
                else
                {
                    ErrorEvent?.Invoke(this, AppResources.Error_Message);
                }
                IsBusy = false;
            }
            catch (Exception ex)
            {
                IsBusy = false;
                Debug.WriteLine(AppResources.ExceptionHeaderMessage + ex.Message);
            }

            finally
            {
                Debug.WriteLine("In Finally");
                IsRefreshing = false;
            }
        }
        public async void LoadHistoryData()
        {
            GetQuoteHistoryRequest req = new GetQuoteHistoryRequest();

            try
            {
                IsBusy = true;
                var res = await req.Execute();

                List<QuotationRequestModel> quotelist = new List<QuotationRequestModel>();

                if (res != null)
                {
                    if (res.data.list.Count > 0)
                    {
                        quotemodel = new QuotationRequestModel();
                        foreach (var quoteitem in res.data.list)
                        {
                            if (quoteitem.categoryCode == VehicleCategoryConstants.NEWFOURWHEEL)
                            {
                                quotemodel.QuoteReqImage = VehicleImageConstants.FOURWHEELIMAGE;
                            }
                            else if (quoteitem.categoryCode == VehicleCategoryConstants.NEWTWOWHEEL)
                            {
                                quotemodel.QuoteReqImage = VehicleImageConstants.TWOWHEELIMAGE;
                            }

                            if (quoteitem.isGiven)
                            {
                                //Green Color
                                quotemodel.StatusColor = "#11A442";
                                quotemodel.QuoteStatus = "Quote is given";
                            }

                            else
                            {
                                //Red Color
                                quotemodel.StatusColor = "#FF0000";
                                quotemodel.QuoteStatus = "Quote is not given";
                            }

                            quotelist.Add(new QuotationRequestModel
                            {
                                QuoteID = quoteitem.quoteId.ToString(),
                                CustomerName = quoteitem.fullname,
                                QuoteReqDateTime = quoteitem.createdDate.ToString(),
                                QuoteClosedStatus = quoteitem.status,
                                PreferTimeSlot = DateTimeConverter.TimeSlotConvert(quoteitem.timeSlot.from, quoteitem.timeSlot.to),
                                QuoteStatus = quotemodel.QuoteStatus,
                                StatusColor = quotemodel.StatusColor,
                                QuoteReqImage = quotemodel.QuoteReqImage,
                                PartnerRemarks = quoteitem.partnerRemarks,
                                VehicleModelName = quoteitem.productName,
                                ModelColor = quoteitem.varientColor,
                            });
                        }
                        quotereqitems.ReplaceRange(quotelist);
                    }
                    else
                    {
                        NoDataEvent?.Invoke(this, null);
                    }
                }
                else
                {
                    ErrorEvent?.Invoke(this, AppResources.Error_Message);
                }
                IsBusy = false;
            }
            catch (Exception ex)
            {
                IsBusy = false;
                Debug.WriteLine(AppResources.ExceptionHeaderMessage + ex.Message);
            }

            finally
            {
                Debug.WriteLine("In Finally");
                IsRefreshing = false;
            }
        }
    }
}
