﻿using EM1_Dealer.Models;
using EM1_Dealer.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM1_Dealer.ViewModel.Payment
{
    public class PaymentRequestViewModel : BaseViewModel
    {
        public ObservableRangeCollection<PaymentRequestModel> paymentreqitems { get; set; }
        public ObservableCollection<Payment_Status> PaymentStatusItems { get; set; }
        public ObservableCollection<Payment_Status> PaymentReqStatusItems { get; set; }
        public List<PaymentRequestModel> PayReqItems { get; set; }

        public PaymentRequestViewModel()
        {
            #region PaymentHistoryStatus
            PaymentStatusItems = new ObservableCollection<Payment_Status>(new[]
            {
                new Payment_Status { PaymentStatusID=1,PaymentTypeFilterText="Booking Amount",PaymentFilter="Booking" },
                new Payment_Status { PaymentStatusID=2,PaymentTypeFilterText="Full Payment",PaymentFilter="Full" },
                new Payment_Status { PaymentStatusID=3,PaymentTypeFilterText="Manual Payment",PaymentFilter="Manual" },
            });
            #endregion

            #region PaymentRequest Status
            PaymentReqStatusItems = new ObservableCollection<Payment_Status>(new[]
            {
                new Payment_Status { PaymentStatusID=1,PaymentTypeFilterText="Booking Amount",PaymentFilter="Booking" },
                new Payment_Status { PaymentStatusID=2,PaymentTypeFilterText="Full Payment",PaymentFilter="Full" },
            });
            #endregion

            #region AllPaymentReq
            paymentreqitems = new ObservableRangeCollection<PaymentRequestModel>(new[]
            {
                new PaymentRequestModel { CustomerName="Customer 1",PaymentReqDateTime="01/17/2018 19:56PM",PaymentReqImage="car_gray.png",VehicleModelName="Mercedes-Benz GLC-Class 2016",VehicleVersionName="220D 4MATIC STYLE",ModelColor="Iridium Silver",PaymentStatus="Accepted",StatusColor="#11A442",PaymentAmount="\u20B9"+" 10 lakh",PaymentTypeText="Booking Amount",PaymentType="Booking",PaymentCode=" "},
                new PaymentRequestModel { CustomerName="Customer 2",PaymentReqDateTime="01/18/2018 08:06AM",PaymentReqImage="scooter_gray.png",VehicleModelName="Royal Enfield Classic 350 2016",VehicleVersionName="350cc BSIV",ModelColor="Diamond Silver Metalic",PaymentStatus="Rejected",StatusColor="#FF0000",PaymentAmount="\u20B9"+" 1.85 lakh",PaymentTypeText="Full Payment",PaymentType="Full",PaymentCode=" "},
                new PaymentRequestModel { CustomerName="Customer 3",PaymentReqDateTime="01/15/2018 09:12AM",PaymentReqImage="car_gray.png",VehicleModelName="Maruti Swift 2015",VehicleVersionName="Vxi Petrol",ModelColor="Polar White",PaymentStatus="Rejected",StatusColor="#FF0000",PaymentAmount="\u20B9"+" 4.97 lakh",PaymentTypeText="Full Payment",PaymentType="Manual",PaymentCode="123456" },
                new PaymentRequestModel { CustomerName="Customer 4",PaymentReqDateTime="01/17/2018 16:02PM",PaymentReqImage="scooter_gray.png",VehicleModelName="Honda Activa 125 2013",VehicleVersionName="3G 125cc",ModelColor="Cherry Red",PaymentStatus="Accepted",StatusColor="#11A442",PaymentAmount="\u20B9"+" 65k",PaymentTypeText="Full Payment",PaymentType="Manual",PaymentCode="123123" },
            });

            #endregion

            PayReqItems = new List<PaymentRequestModel>();
            foreach (var payitems in paymentreqitems)
            {
                if(payitems.PaymentType == "Booking" || payitems.PaymentType == "Full")
                {
                    PayReqItems.Add(payitems);
                }
            }
        }
    }
}
