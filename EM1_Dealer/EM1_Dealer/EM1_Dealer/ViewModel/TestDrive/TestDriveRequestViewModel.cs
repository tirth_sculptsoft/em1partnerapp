﻿using EM1_Dealer.Enums;
using EM1_Dealer.Helpers;
using EM1_Dealer.Models;
using EM1_Dealer.Request;
using EM1_Dealer.Resources;
using EM1_Dealer.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace EM1_Dealer.ViewModel.TestDrive
{
    public class TestDriveRequestViewModel : BaseViewModel
    {
        public TestDriveRequestModel TDReqModel = null;
        public EventHandler<string> NoDataEvent;

        public ObservableRangeCollection<TestDrive_Status> TDStatusItems { get; set; }
        public ObservableRangeCollection<TestDriveRequestModel> testdrivereqitems { get; set; }

        bool _IsRefreshing;
        public bool IsRefreshing
        {
            get
            {
                return _IsRefreshing;
            }
            set
            {
                SetProperty(ref _IsRefreshing, value);
            }
        }

        public ICommand RefreshCommand { protected set; get; }
        public ICommand RefreshHistoryCommand { protected set; get; }


        public TestDriveRequestViewModel()
        {
            testdrivereqitems = new ObservableRangeCollection<TestDriveRequestModel>();

            this.RefreshCommand = new Command(() =>
              {
                  IsBusy = true;
                  LoadData();
              });

            this.RefreshHistoryCommand = new Command(() =>
              {
                  IsBusy = true;
                  LoadHistoryData();
              });

            #region TestDrive StatusList
            TDStatusItems = new ObservableRangeCollection<TestDrive_Status>(new[]
            {
                new TestDrive_Status { TDStatusID=1,TDStatus="Accepted" },
                new TestDrive_Status { TDStatusID=2,TDStatus="Rejected" },
                new TestDrive_Status { TDStatusID=3,TDStatus="Postponed" },
                new TestDrive_Status { TDStatusID=4,TDStatus="Completed" }
            });
            #endregion

            #region Static Data
            //testdrivereqitems = new ObservableRangeCollection<TestDriveRequestModel>(new[]
            //{
            //    new TestDriveRequestModel { TDReqID="TD12345678123", CustomerName="Customer 1",TestReqDate="01/17/2018 19:56PM",TestDriveReqImage="car_gray.png",VehicleModelName="Mercedes-Benz GLC-Class 2016",TDStatus="Completed",StatusColor="#11A442"},
            //    new TestDriveRequestModel { TDReqID="TD12345678134", CustomerName="Customer 2",TestReqDate="01/17/2018 20:30PM",TestDriveReqImage="scooter_gray.png",VehicleModelName="Honda Activa 125 2013",TDStatus="Rejected",StatusColor="#FF0000"}
            //});
            #endregion
        }

        public async void LoadData()
        {
            GetPendingTestDriveRequest req = new GetPendingTestDriveRequest();

            try
            {
                IsBusy = true;
                var res = await req.Excecute();

                List<TestDriveRequestModel> TDReqList = new List<TestDriveRequestModel>();

                if (res != null)
                {
                    if (res.data.list.Count > 0)
                    {
                        TDReqModel = new TestDriveRequestModel();
                        foreach (var TDpendingitem in res.data.list)
                        {
                            var tmptime = TDpendingitem.testDriveTimeSlot;

                            #region VehicleImage
                            if (TDpendingitem.categoryCode == VehicleCategoryConstants.NEWFOURWHEEL)
                            {
                                TDReqModel.TestDriveReqImage = VehicleImageConstants.FOURWHEELIMAGE;
                            }
                            else if (TDpendingitem.categoryCode == VehicleCategoryConstants.NEWTWOWHEEL)
                            {
                                TDReqModel.TestDriveReqImage = VehicleImageConstants.TWOWHEELIMAGE;
                            }
                            #endregion

                            TDReqList.Add(new TestDriveRequestModel
                            {
                                TDReqID = TDpendingitem.testDriveId,
                                CustomerName = TDpendingitem.customerName,
                                TDScheduleDate = TDpendingitem.testDriveDate.ToString(),
                                TestReqDateTime = TDpendingitem.createdDate.ToString(),
                                PreferTestDriveTimeSlot = DateTimeConverter.TimeSlotConvert(TDpendingitem.testDriveTimeSlot.from, TDpendingitem.testDriveTimeSlot.to),
                                TDAddress = TDpendingitem.address,
                                PreferCallTimeSlot = DateTimeConverter.TimeSlotConvert(TDpendingitem.timePreferences.from, TDpendingitem.timePreferences.to),
                                VehicleModelName = TDpendingitem.productName,
                                TestDriveReqImage = TDReqModel.TestDriveReqImage,
                                TDStatus = TDpendingitem.status
                            });
                        }

                        testdrivereqitems.ReplaceRange(TDReqList);
                    }
                    else
                    {
                        NoDataEvent?.Invoke(this, null);
                    }
                }
                else
                {
                    ErrorEvent?.Invoke(this, AppResources.Error_Message);
                }

                IsBusy = false;
            }
            catch (Exception ex)
            {
                IsBusy = false;
                Debug.WriteLine(AppResources.ExceptionHeaderMessage + ex.Message);
            }

            finally
            {
                Debug.WriteLine("In Finally");
                IsRefreshing = false;
            }
        }

        public async void LoadHistoryData()
        {
            GetTestDriveHistoryRequest req = new GetTestDriveHistoryRequest();

            try
            {
                IsBusy = true;
                var res = await req.Execute();

                List<TestDriveRequestModel> TDHistoryList = new List<TestDriveRequestModel>();

                if (res != null)
                {
                    if (res.data.list.Count > 0)
                    {
                        TDReqModel = new TestDriveRequestModel();
                        foreach (var TDHistoryitem in res.data.list)
                        {
                            #region VehicleImage
                            if (TDHistoryitem.categoryCode == VehicleCategoryConstants.NEWFOURWHEEL)
                            {
                                TDReqModel.TestDriveReqImage = VehicleImageConstants.FOURWHEELIMAGE;
                            }
                            else if (TDHistoryitem.categoryCode == VehicleCategoryConstants.NEWTWOWHEEL)
                            {
                                TDReqModel.TestDriveReqImage = VehicleImageConstants.TWOWHEELIMAGE;
                            }
                            #endregion

                            TDHistoryList.Add(new TestDriveRequestModel
                            {
                                TDReqID = TDHistoryitem.testDriveId,
                                CustomerName = TDHistoryitem.customerName,
                                TestReqDateTime = TDHistoryitem.createdDate.ToString(),
                                TestDriveReqImage = TDReqModel.TestDriveReqImage,
                                VehicleModelName = TDHistoryitem.productName,
                                TDStatus = TDHistoryitem.status,
                                TDAddress = TDHistoryitem.address,
                                PreferCallTimeSlot = DateTimeConverter.TimeSlotConvert(TDHistoryitem.timePreferences.from, TDHistoryitem.timePreferences.to),
                                PreferTestDriveTimeSlot = DateTimeConverter.TimeSlotConvert(TDHistoryitem.testDriveTimeSlot.from, TDHistoryitem.testDriveTimeSlot.to),
                                TDScheduleDate = TDHistoryitem.testDriveDate.ToString(),
                                StatusColor = "#11A442"
                            });
                        }

                        testdrivereqitems.ReplaceRange(TDHistoryList);
                    }
                    else
                    {
                        NoDataEvent?.Invoke(this, null);
                    }
                }
                else
                {
                    ErrorEvent?.Invoke(this, AppResources.Error_Message);
                }

                IsBusy = false;
            }
            catch (Exception ex)
            {
                IsBusy = false;
                Debug.WriteLine(AppResources.ExceptionHeaderMessage + ex.Message);
            }

            finally
            {
                Debug.WriteLine("In Finally");
                IsRefreshing = false;
            }
        }
    }
}
