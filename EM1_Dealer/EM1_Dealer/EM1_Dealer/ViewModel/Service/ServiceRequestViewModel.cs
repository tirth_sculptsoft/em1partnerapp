﻿using EM1_Dealer.Helpers;
using EM1_Dealer.Models;
using EM1_Dealer.Resources;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace EM1_Dealer.ViewModel.Service
{
    public class ServiceRequestViewModel : BaseViewModel
    {
        public EventHandler<string> NoDataEvent;
        public ObservableRangeCollection<ServiceRequestModel> servicereqitems { get; set; }

        bool _IsRefreshing;
        public bool IsRefreshing
        {
            get
            {
                return _IsRefreshing;
            }
            set
            {
                SetProperty(ref _IsRefreshing, value);
            }
        }

        public ICommand RefreshCommand { protected set; get; }
        public ICommand RefreshHistoryCommand { protected set; get; }

        public ServiceRequestViewModel()
        {
            servicereqitems = new ObservableRangeCollection<ServiceRequestModel>();

            this.RefreshCommand = new Command(() =>
            {
                IsBusy = true;
                LoadData();
            });
        }

        public void LoadData()
        {
            try
            {
                servicereqitems = new ObservableRangeCollection<ServiceRequestModel>(new[]
                {
                    new ServiceRequestModel { ServiceID=1,CustomerName="Customer 1",ServiceScheduleDate="06/03/2018 03:46PM",VehicleModelName="Mercedes-Benz GLC-Class 2016" },
                    new ServiceRequestModel { ServiceID=1,CustomerName="Customer 2",ServiceScheduleDate="07/03/2018 02:34PM",VehicleModelName="Maruti Suzuki Celerio X 2016" },
                });
            }

            catch (Exception ex)
            {
                IsBusy = false;
                Debug.WriteLine(AppResources.ExceptionHeaderMessage + ex.Message);
            }

            finally
            {
                Debug.WriteLine("In Finally");
                IsRefreshing = false;
            }
        }
    }
}
